package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	"golang.org/x/net/context"
	"google.golang.org/api/sheets/v4"
)

// Doll: doll type
type Doll struct {
	name      string
	id        int
	rarity    int
	trueIndex int
	dollType  string
	pros      string
	cons      string
	analysis  string
	tldr      string
}

var dolls map[string]Doll = map[string]Doll{}

// SpreadsheetID: Google Sheets ID
const SpreadsheetID = "10LJdksnM3zipOb72IneJD7WVp3765JYJEGg0LnodzDI"
const (
	PROSCOLUMN     = "O"
	CONSCOLUMN     = "Y"
	ANALYSISCOLUMN = "AI"
	TLDRCOLUMN     = "AK"
)

var acceptedTypes = [6]string{"HG", "SMG", "RF", "AR", "MG", "SG"}

func main() {
	fmt.Println("Hello World")
	// Load .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	botToken := os.Getenv("DISCORDBOTTOKEN")
	discord, err := discordgo.New("Bot " + botToken)
	discord.AddHandler(ready)
	discord.AddHandler(messageReceivedHandler)
	err = discord.Open()
	if err != nil {
		fmt.Println("Error opening Discord session: ", err)
	}
	fmt.Println("Kalina is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	discord.Close()

}

func ready(s *discordgo.Session, event *discordgo.Ready) {

	// Set the playing status.
	s.UpdateStatus(0, "!cash counting")
}

func messageReceivedHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	msg := m.Message.Content
	fmt.Printf("[LOG] Message received: %s\n", msg)
	commandMatch := regexp.MustCompile(`(/k) (.*) (.*)`).FindStringSubmatch(msg)
	if len(commandMatch) == 0 {
		// s.ChannelMessageSend(m.Message.ChannelID, "")
		fmt.Println("[LOG] No targeted at Kalina. Ignore.")
	} else if len(commandMatch) < 4 {
		s.ChannelMessageSend(m.Message.ChannelID,
			`
			Shikikan-sama! That is not a valid command!
			You need to put it in the format of /k [dollType] [dollName].
			`,
		)
	} else {
		dollType := commandMatch[2]
		fmt.Printf("[LOG] dollType: %s\n", string(dollType))
		if !validDollType(dollType) {
			s.ChannelMessageSend(m.Message.ChannelID,
				`
Shikikan-sama! That is not a valid doll type!
Valid doll types are: [HG, SMG, RF, AR, MG, SG].
`,
			)
			return
		}

		dollName := commandMatch[3]
		fmt.Printf("[LOG] dollName: %s\n", dollName)

		srv, err := sheets.NewService(context.Background())
		if err != nil {
			log.Fatalf("Unable to retrieve Sheets client: %v", err)
		}

		readRange := fmt.Sprintf("%s!B6:B", dollType)
		resp, err := srv.Spreadsheets.Values.Get(SpreadsheetID, readRange).Do()
		if len(resp.Values) == 0 {
			fmt.Println("No data found.")
		} else {
			for index, row := range resp.Values {
				if len(row) == 0 {
					continue
				} else {
					var doll = populateDoll(index, row, dollType)
					dolls[doll.name] = doll
				}
			}
			if targetDoll, ok := dolls[dollName]; ok && (dolls[dollName].dollType == dollType) {
				fmt.Printf("[LOG] Target doll %s\nID: %d\nRarity:%d\n", targetDoll.name, targetDoll.id, targetDoll.rarity)
				prosRange := fmt.Sprintf("%s!%s%d", dollType, PROSCOLUMN, targetDoll.trueIndex)
				prosRes, err := srv.Spreadsheets.Values.Get(SpreadsheetID, prosRange).Do()
				if err != nil {
					log.Fatalf("ERROR fetching Pros for %s: %v", targetDoll.name, err)
				}
				targetDoll.pros = fmt.Sprintf("```%s```", prosRes.Values[0][0].(string))

				consRange := fmt.Sprintf("%s!%s%d", dollType, CONSCOLUMN, targetDoll.trueIndex)
				consRes, err := srv.Spreadsheets.Values.Get(SpreadsheetID, consRange).Do()
				if err != nil {
					log.Fatalf("ERROR fetching Cons for %s: %v", targetDoll.name, err)
				}
				targetDoll.cons = fmt.Sprintf("```%s```", consRes.Values[0][0].(string))

				anaRange := fmt.Sprintf("%s!%s%d", dollType, ANALYSISCOLUMN, targetDoll.trueIndex+1)
				anaRes, err := srv.Spreadsheets.Values.Get(SpreadsheetID, anaRange).Do()
				if err != nil {
					log.Fatalf("ERROR fetching Analysis for %s: %v", targetDoll.name, err)
				}
				targetDoll.analysis = anaRes.Values[0][0].(string)

				tldrRange := fmt.Sprintf("%s!%s%d", dollType, TLDRCOLUMN, targetDoll.trueIndex)
				tldrRes, err := srv.Spreadsheets.Values.Get(SpreadsheetID, tldrRange).Do()
				if err != nil {
					log.Fatalf("ERROR fetching TLDR for %s: %v", targetDoll.name, err)
				}
				targetDoll.tldr = fmt.Sprintf("```%s```", tldrRes.Values[0][0].(string))

				// dollname, id, dolltype, rarity, pros, cons,
				_, err = s.ChannelMessageSend(
					m.Message.ChannelID,
					fmt.Sprintf("**%s**\nNo.%d\n__%s__\nRarity: %d\n", targetDoll.name, targetDoll.id, dollType, targetDoll.rarity),
				)
				if err != nil {
					log.Fatalf("ERROR sending message for %s: %v", targetDoll.name, err)
				}
				_, err = s.ChannelMessageSend(
					m.Message.ChannelID,
					fmt.Sprintf("**PROs**\n%s\n**CONS**\n%s", targetDoll.pros, targetDoll.cons),
				)
				if err != nil {
					log.Fatalf("ERROR sending message for %s: %v", targetDoll.name, err)
				}

				_, err = s.ChannelMessageSend(m.Message.ChannelID, "**Analysis**")
				analysisSlice := strings.Split(targetDoll.analysis, "\n")

				for _, mAnalysis := range analysisSlice {
					if mAnalysis == "" {
						continue
					}
					_, err = s.ChannelMessageSend(m.Message.ChannelID, fmt.Sprintf("> %s", mAnalysis))
					if err != nil {
						log.Fatalf("ERROR sending message for %s: %v", targetDoll.name, err)
					}

				}
				_, err = s.ChannelMessageSend(m.Message.ChannelID, fmt.Sprintf("**TL;DR**\n%s", targetDoll.tldr))
				if err != nil {
					log.Fatalf("ERROR sending message for %s: %v", targetDoll.name, err)
				}
				return
			} else {
				s.ChannelMessageSend(m.Message.ChannelID, "Shikikan-sama! That doll does not exist!")
			}
		}
	}

}

func validDollType(providedDollType string) bool {
	for _, mDollType := range acceptedTypes {
		if providedDollType == mDollType {
			return true
		}
	}
	return false
}

func populateDoll(index int, row []interface{}, dollType string) Doll {
	var trueIndex int = index + 6
	var doll = Doll{}
	strs := strings.Split(row[0].(string), "\n")
	for _, str := range strs {
		if regexp.MustCompile(`\(NO. ?[0-9]+\)`).MatchString(str) {
			doll.id, _ = strconv.Atoi(str[5 : len(str)-1])
		} else if regexp.MustCompile(`[★]+`).MatchString(str) {
			doll.rarity = strings.Count(str, "★")
		} else {
			doll.name += str
		}
	}
	doll.trueIndex = trueIndex
	doll.dollType = dollType
	return doll
}
