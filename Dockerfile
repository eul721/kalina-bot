FROM golang:latest
WORKDIR /kalina
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY main.go /kalina
RUN go build -o kalina
ENTRYPOINT ["./kalina"]
