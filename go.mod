module gitlab.com/eul721/kalina

go 1.13

require (
	cloud.google.com/go v0.46.3 // indirect
	github.com/bwmarrin/discordgo v0.19.0
	github.com/golang/groupcache v0.0.0-20191002201903-404acd9df4cc // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/joho/godotenv v1.3.0
	go.opencensus.io v0.22.1 // indirect
	golang.org/x/net v0.0.0-20191007182048-72f939374954
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/sys v0.0.0-20191007154456-ef33b2fb2c41 // indirect
	google.golang.org/api v0.10.0
	google.golang.org/appengine v1.6.5 // indirect
	google.golang.org/genproto v0.0.0-20191007204434-a023cd5227bd // indirect
	google.golang.org/grpc v1.24.0 // indirect
)
